<?php
namespace Sion;

/**
 * Que el Señor te bendiga continuamente desde Sión; 
 * que veas prosperar a Jerusalén durante toda tu vida.
 * Salmo 128:5
 *
 * Clase con metodos tipo herramientas miselaneos para apoyar proceso de las aplicaciones
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class ToolsSion
{
    public static function isHex(string $hex): bool
    {
        return \ctype_xdigit($hex);
    }

    public static function isEmail(string $string): bool
    {
        return \filter_var($string, FILTER_VALIDATE_EMAIL) ? true : false;
    }

    public static function isNumeric(string $str): bool
    {
        return \is_numeric($str);
    }

    public static function isURL(string $string): bool
    {
        return (filter_var($string, FILTER_VALIDATE_URL) !== false) ? true : false; 
    }

    public static function datatype(string $string): array
    {
        $phonePattern = '#^\+?\d{1,3}?[- .]?\(?(?:\d{2,3})\)?[- .]?\d\d\d[- .]?\d\d\d\d$#';
        $string = static::stToCase($string, 'lower');
        $data = ['type' => 'string', 'data' => $string];
        if (static::isEmail($string)) {
            $data['type'] = 'email';
        } elseif (static::isNumeric($string)) {
            $data['type'] = 'number';
        } elseif (static::isUrl($string)) {
            $data['type'] = 'url';
        } elseif (preg_match($phonePattern, $string)){
            $data['type'] = 'phone';
        } elseif (static::isHex($string)) {
            $data['type'] = 'hex';
        }
        return $data;
    }

    /**
     * Devolver un string en mayúsculas o minúsculas
     */
    public static function stToCase(string $str, string $case = 'upper'): string
    {
        return $case == 'upper' ? \mb_convert_case($str, MB_CASE_UPPER, 'UTF-8') : \mb_convert_case($str, MB_CASE_LOWER, 'UTF-8');
    }

    /**
     * Quitar los acentos a un string
     */
    public static function noAccents(string $str, bool $enie = false): string
    {
        if($enie){
            $noPermitidas = ["á","é","í","ó","ú","Á","É","Í","Ó","Ú","ñ","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹"];
            $permitidas = ["a","e","i","o","u","A","E","I","O","U","n","N","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E"];
        }else{
            $noPermitidas = ["á","é","í","ó","ú","Á","É","Í","Ó","Ú","À","Ã","Ì","Ò","Ù","Ã™","Ã ","Ã¨","Ã¬","Ã²","Ã¹","ç","Ç","Ã¢","ê","Ã®","Ã´","Ã»","Ã‚","ÃŠ","ÃŽ","Ã”","Ã›","ü","Ã¶","Ã–","Ã¯","Ã¤","«","Ò","Ã","Ã„","Ã‹"];
            $permitidas= ["a","e","i","o","u","A","E","I","O","U","A","E","I","O","U","a","e","i","o","u","c","C","a","e","i","o","u","A","E","I","O","U","u","o","O","i","a","e","U","I","A","E"];
        }
        return \str_replace($noPermitidas, $permitidas, $str);
    }

    /**
     * Quita puntos, comas, punto y coma y dos puntos
     */
    public static function removePunctuation(string $string, bool $all = false): string
    {
        return $all ? str_replace(['.', ',', ';', ':'], '', $string) : str_replace(['.', ','], '', $string);
    }

    public static function cleanSpaces(string $str): string
    {
        $array = explode(' ',trim($str));
        return self::stArraySentence($array);
    }

    public static function stAllTrim(string $str): string
    {
        return str_replace(' ', '', $str);
    }

    public static function stArraySentence(array $array): string
    {
        $nwString = '';
        foreach ($array as $itm) {
            if(strlen($itm) > 0){
                $nwString.=' '.$itm;
            }
        }
        return trim($nwString);
    }

    /**
     * Para agregar un cero a la izquierda cuando un número es de un solo dígito  
     */
    public static function zeroPadding(int $number): string
    {
        return \str_pad($number, 2, "0", STR_PAD_LEFT);
    }

    public static function stBase64(string $str): array
    {
        $file = explode(';base64,', $str);
        if (count($file) == 2){
            return ['type' => str_replace('data:','',$file[0]), 'file' => $file[1]];
        }
        return [];
    }

    public static function arrayExtract(array $keys, array $vals): array
    {
        $nArray = [];
        foreach($keys as $ky){ 
            array_key_exists($ky, $vals) ? $nArray[$ky] = $vals[$ky] : $nArray[$ky] = null;
        }
        return $nArray;
    }

    public static function alphanumeric(string $string = null, string $case = null): string
    {
        $str = preg_replace(sprintf('~[^%s]++~i','0-9a-z'), '', $string);
        if ($case == 'upper') {  
            return self::stToCase($str);
        } elseif ($case == 'lower'){ 
            return self::stToCase($str, 'lower');
        }
        return $str;
    }

    public static function getAlphanumeric(string $string = null, bool $upper = false): string
    {
        $str = preg_replace(sprintf('~[^%s]++~i','0-9a-z'), '', $string);
        return $upper ? self::stToCase($str) : $str;
    }

    public static function getAlphabetical(string $string, bool $upper = false): string
    {
        $str = preg_replace('/[\d]/', '', $string);
        $dic = ['+', '-', '\\', '/', '*', '_', '.', ':', ',', ';', '@', '#', '$', '%', '&', '~', '|', '^', '"', '\'', '¿', '?', '!', '¡', '(', ')', '{', '}','[', ']', '<', '>'];
        $str = str_replace($dic, '', $str);
        return $upper ? self::stToCase($str) : $str;
    }

    public static function getNumber(string $string): string
    {
        return preg_replace('/[^0-9]+/', '', $string);
    }

    public static function getEmail(string $string): string
    {
        $email = self::stToCase(self::cleanSpaces($string), 'lower');
        return self::isEmail($email) ? $email : '';
    }

    public static function getPhone(string $number, int $length = 10): string
    {
        return \substr(self::getNumber($number), "-{$length}");
    }

    public static function getTitle(string $string): string
    {
        return mb_convert_case(self::cleanSpaces($string), MB_CASE_TITLE,'UTF-8');
    }
    
    public static function getName(string $str, bool $noAccents = false): string
    {
        $s = [' De ', ' Del '];
        $r = [' de ', ' del '];
        $name = str_replace($s, $r, self::getTitle(self::getAlphabetical($str)));
        return $noAccents ? self::noAccents($name) : $name;
    }

    public static function getPlaceName(string $str): string
    {
        $s = [' De ', ' Del ', ' D.c', ' D.f'];
        $r = [' de ', ' del ', ' D.C', ' D.F'];
        $name = self::getTitle($str);
        return \str_replace($s, $r, $name);
    }

    /**
     * Devolver un string con el formato de un nombre de usuario (sin espacios, acentos y en minusculas)
     */
    public static function getUsername(string $username): string
    {
        return static::stToCase(static::cleanSpaces(static::noAccents($username)), 'lower');
    }

    public static function getCompany(string $str): string
    {
        $s = [' De ', ' Del ', 'Ltda', 'Ltda.', 'Sas', 'S.a.s',  'S.a.s.', 'Sa', 'S.a', 'S.a.'];
        $r = [' de ', ' del ', 'LTDA', 'LTDA',  'S.A.S', 'S.A.S', 'S.A.S', 'S.A', 'S.A', 'S.A'];
        $name = self::getTitle($str);
        return \str_replace($s, $r, $name);
    }

    public static function getPorcentajeSimilitud(string $string1, string $string2): int
    {
        $porcentaje = 0;
        \similar_text($string1, $string2, $porcentaje);
        return \round($porcentaje);
    }

    public static function arrayFlatten(array $array): array
    {
        $return = [];
        foreach ($array as $key => $value) {
            if (\is_array($value)) {
                $return = \array_merge($return, self::arrayFlatten($value));
            } else {
                $return[$key] = $value;
            }
        }
        return $return;
    }
    /**
     * devuelve array con los keys separados por . segunsu profundidad con datos
     * [
     *  'key' => 'v1'
     *  'key.a' => 'v2',
     *  'key.b.x' => 'v3'
     * ]
     */
    public static function arrayFlattenKey(array $array, string $base = ''): array
    {
        $return = [];
        foreach($array as $key => $value){
            $bkey = $base ? "$base.$key" : $key;
            if (\is_array($value)){
                $return = \array_merge($return, self::arrayFlattenKey($value, $bkey));
            } else {
                $return[$bkey] = $value;
            }
        }
        return $return;
    }

    /**
     * Transforma un array, buscas los keys y pasa sus valores a los nuevos keys ($v) 
     * Devuelve el array con los keys nuevos
     */
    public static function transformData(array $arrayKeys, array $data): array
    {
        $keys = [];
        foreach($arrayKeys as $k => $v){
            $keys[] = $v;
            if(!empty($data[$k])){
                $data[$v] = $data[$k];
            }
        }
        return self::arrayExtract($keys, $data);
    }

    public static function round($num, int $precision, int $mode = PHP_ROUND_HALF_UP)
    {
        return \round($num, $precision, $mode);
    }
}