<?php
namespace Sion;

/**
 * Que el Señor te bendiga continuamente desde Sión; 
 * que veas prosperar a Jerusalén durante toda tu vida.
 * Salmo 128:5
 * 
 * Metodos para obtener información del cliente
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class ClientSion 
{
    public const SOS = ['android'=>'Android','iphone'=>'iOS','ipad'=>'iOS','ubuntu'=>'Linux','windows phone'=>'Windows Phone','linux'=>'Linux','macintosh'=>'Macintosh','windows'=>'Windows','CrOS'=>'Chrome OS'];
    public const BROWSER = ['edge'=>'Edge','opr'=>'Opera','firefox'=>'Firefox','chrome'=>'Chromium','safari'=>'Safari'];

    /**
     * Devuelve la primera direccion que es la del cliente, las demás son los proxy 
     */
    public static function getIpClient(string $strIp): string
    {
        $ip = \explode(',', $strIp);
        return !empty($ip[0]) ? \str_replace(' ', '', $ip[0]) : '';
    }

    /**
     * Devuelve ip address del cliente 
     */
    public static function ipClient(): string
    {
        return isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] ? self::getIpClient($_SERVER['HTTP_X_FORWARDED_FOR']) : self::getIpClient($_SERVER['REMOTE_ADDR']);
    }

    /**
     * Devuelve la información del useragent
     */
    public static function userAgent(): string
    {
        return isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
    }

    /**
     * Devuelve un sha1 de userAgent
     */
    public static function userAgentId(): string
    {
        return \sha1(self::userAgent());
    }

    /**
     * Intenta reconocer cual navegador es el que está usando el cliente según el useragent
     */
    public static function browser(string $userAgent = null): string
    {
        $browser = 'Otro';
        if(!$userAgent){ 
            $userAgent = self::userAgent(); 
        }
        foreach(array_keys(self::BROWSER) as $brw){
            if(stristr($userAgent, $brw)){ 
                $browser = self::BROWSER[$brw]; 
                break; 
            }
        }
        return $browser;
    }

    /**
     * Intenta reconocer cual Sistema Operativo tiene el cliente según el useragent
     */
    public static function SO(string $userAgent = null): string
    {
        $so = 'Otro';
        $userAgent = $userAgent?: self::userAgent();
        foreach(array_keys(self::SOS) as $itm){
            if(stristr($userAgent, $itm)){ 
                $so = self::SOS[$itm]; 
                break; 
            }
        }
        return $so;
    }
}