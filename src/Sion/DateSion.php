<?php
namespace Sion;

/**
 * ¡Glorifica al Señor, oh Jerusalén! ¡Alaba a tu Dios, oh Sión!
 * Salmos 147:12
 * 
 * Clase que permite trabajar con fechas
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */

class DateSion
{
    const MONTHS = [
        'es' => ['01' => 'Enero', '02' => 'Febrero', '03' => 'Marzo', '04' => 'Abril', '05' => 'Mayo', '06' => 'Junio', '07' => 'Julio', '08' => 'Agosto', '09' => 'Septiembre', '10' => 'Octubre', '11' => 'Noviembre', '12' => 'Diciembre' ],
        'en' => ['01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December' ]
    ];

    /**
     * Evalua si es una fecha, es obsoleto el null como parametro de dateTime 
     */
    public static function getIsDate(string $date = null, string $timezone = 'America/Bogota'): ?\DateTime
    {
        try { 
            $date = $date ?: 'now'; 
            return new \DateTime($date, new \DateTimeZone($timezone)); 
        } catch (\Exception $e) { 
            return null; 
        }
    }

    /**
     * Valida si un string de fecha es válido como fecha y corresponde al formato indicado
     */
    public static function isDate(string $date, string $format = 'Y-m-d H:i:s'): bool
    {
        $d = \DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * @deprecated date() is used instead
     * @return \DateTime - string format
     */
    public static function stDate(string $strDate = null, string $format = 'DateTime')
    {
        return self::date($strDate, $format);
    }

    /**
     * @return \DateTime - string format
     */
    public static function date(string $strDate = null, string $format = 'DateTime', string $timezone = 'America/Bogota')
    {
        if(!$date = self::getIsDate($strDate, $timezone)){
            $date = new \DateTime(date("Y-m-d H:i:s"), new \DateTimeZone($timezone));
        }
        return $date && ($format == 'DateTime' || $format == 'object') ? $date : $date->format($format);
    }

    /**
     * @return timestamp
     */
    public static function getTimestamp(string $strDate): int
    {
        return \strtotime($strDate);
    }

    /**
     * devuelve un string de una fecha para un timestamp
     */
    public static function timestampToStringDate(int $timeStamp, $format = "Y-m-d H:i:s"): ?string
    {
        try{
            return \date($format, $timeStamp);
        }catch(\Exception $e){
            return null;
        }
    }

    public static function timestampToDateTime(int $timestamp): ?\DateTime
    {
        $strDate = self::timestampToStringDate($timestamp, "Y-m-d H:i:s");
        return $strDate ? self::date($strDate) : null;
    }

    /**
     * @deprecated sum() is used instead
     * @return \DateTime || string format
     */
    public static function stSum(string $t, int $n, string $strDate = null, bool $add = true, string $fmt = 'Y-m-d')
    {
        return self::sum($t, $n, $strDate, $add, $fmt);
    }

    /**
     * @return \DateTime || string format
     */
    public static function sum(string $t, int $n, string $strDate = null, bool $add = true, string $fmt = 'DateTime')
    {
        $type = ['segundos'=>'seconds','seconds'=>'seconds','minutos'=>'minutes','minutes'=>'minutes','horas'=>'hours','hours'=>'hours','dia'=>'day','dias'=>'day','day'=>'day','semana'=>'week','semanas'=>'week','week'=>'week','mes'=>'month','month'=>'month','meses'=>'month', 'anios'=>'year', 'year'=>'year'];
        $tp = 'day';
        if(!$strDate){ 
            $strDate = static::date()->format("Y-m-d H:i:s");
        }
        if(isset($type[$t])){
            $tp = $type[$t];
        }
        if($fmt == 'object' || $fmt == 'DateTime'){
            $str = $add ? \date("Y-m-d H:i:s", \strtotime("+$n $tp", \strtotime($strDate))) : \date("Y-m-d H:i:s", \strtotime("-$n $tp", \strtotime($strDate)));
            return static::date($str);
        }
        return $add ? \date($fmt, \strtotime("+$n $tp", \strtotime($strDate))) : \date($fmt, \strtotime("-$n $tp", \strtotime($strDate)));
    }

    /**
     * Devuelve array con la fecha del lunes y del domingo tomando como referencia un string fecha
     */
    public static function startEndWeek(string $strDate = null, string $fmt = 'DateTime'): array
    {
        $week = [];
        if($date = self::date($strDate)){
            $days2Mon = $date->format('w') == 0 ? 6 : ($date->format('w') -1);
            $days2Sun = $date->format('w') == 0 ? 0 : (-1 * $date->format('w') +7);
            $week = [
                'monday' => self::sum('day', $days2Mon, $date->format("Y-m-d H:i:s"), false, $fmt),
                'sunday' => self::sum('day', $days2Sun, $date->format("Y-m-d H:i:s"), true, $fmt)
            ];
        }
        return $week;
    }

    /**
     * Devuelve array de todas las fechas de una semana para una fecha
     */
    public static function datesWeek(string $strDate = null, string $fmt = 'DateTime'): array
    {
        $week = [
            'lunes' => '',
            'martes' => '',
            'miercoles' => '',
            'jueves' => '',
            'viernes' => '',
            'sabado' => '',
            'domingo' => '',
        ];
        $date = self::date($strDate);
        $dayDiff = $date->format('w') == 0 ? 6 : ($date->format('w') -1);
        $date = self::sum('day', $dayDiff, $date->format("Y-m-d H:i:s"), false);
        $day = 0;
        foreach($week as $dy => $dt){
            $week[$dy] = self::sum('day', $day, $date->format("Y-m-d H:i:s"), true, $fmt);
            $day++;
        }
        return $week;
    }

    /**
     * Devuelve el último día-fecha de un mes de un año determinado en formato indicado
     */
    public static function endDay(int $y, int $m, $fmt = 'd')
    {
        return self::date(\date("Y-m-d H:i:s", (mktime(0, 0, 0, $m + 1, 1, $y)-1)), $fmt);
    }

    /**
     * Devuelve array con todas las fechas desde una fechaInicio hasta una fechaFin en el formato indicado
     */
    public static function dates(string $dateStart, string $dateEnd, $fmt = 'DateTime'): array
    {
        $dates = [];
        $start = self::date($dateStart);
        $end = self::date($dateEnd);
        if($start && $end){
            $days = $start->diff($end)->days;
            $dates[] = self::date($dateStart, $fmt);
            for($i = 1; $i <= $days; $i++){
                $dates[] = self::sum('day', $i, $dateStart, true, $fmt);
            }
        }
        return $dates;
    }

    public static function getMonth(string $lang = 'es', string $mnt = null)
    {
        if ($mnt) {
            return isset(self::MONTHS[$lang][$mnt]) ? self::MONTHS[$lang][$mnt] : $mnt;
        }
        return isset(self::MONTHS[$lang]) ? self::MONTHS[$lang] : [];
    }

    /**
     * Devuelve un array con id, name para los meses del año
     */
    public static function getArrayMonths(string $lang = 'es'): array
    {
        $months = [];
        $data = isset(self::MONTHS[$lang]) ? self::MONTHS[$lang] : self::MONTHS['es'];
        foreach ($data as $key => $val) {
            $months[] = ['id' => $key, 'name' => $val];
        }
        return $months;
    }

    /**
     * Devuelve una fecha para leer en español o en ingles
     */
    public static function dateLang(string $lang = 'es', string $strDate = null): string
    {
        if($date = self::date($strDate)){
            $y = $date->format("Y");
            $d = $date->format("d");
            $m = self::getMonth($lang, $date->format("m"));
            return $lang == 'es' ? "$m $d de $y" : "$d $m $y";
        }
        return '';
    }

    public static function yearsRange(int $ageMin, int $ageMax, string $sort = 'ASC', string $strDate = null): array
    {
        $years = [];
        $date = $strDate ? self::date($strDate) : self::date();
        $yMax = self::sum('year', $ageMax, $date->format("Y-m-d"), false)->format("Y");
        $yMin = self::sum('year', $ageMin, $date->format("Y-m-d"), false)->format("Y");
        for ($i = $yMax; $i <= $yMin; $i++) {
            $years[] = "{$i}";
        }
        if ($sort == 'DESC') {
            \rsort($years);
        }
        return $years;
    }

    /**
     * Devuelve la edad en años a partir de la fecha de nacimiento
     */
    public static function age(string $birthday): string
    {
        return self::date()->diff(self::date($birthday))->y;
    }

    public static function ageRange(int $min, int $max, $strDate = null): array
    {
        $age = [];
        if ($date = self::date($strDate)) { 
            $age['min'] = self::sum('year', $min, $date->format("Y-m-d"), false, 'DateTime');
            $age['max'] = self::sum('year', $max, $date->format("Y-m-d"), false, 'DateTime');

        }
        return $age;
    }

    /**
     * Diferencia entre fechas. Objeto DateInterval
     */
    public static function diff(\DateTime $start, \DateTime $end): \DateInterval
    {
        return $start->diff($end);
    }

    /**
     * Diferencia en días entre dos fechas. Propiedad DateInterval->days
     */
    public static function diffDays(\DateTime $start, \DateTime $end): int
    {
        return self::diff($start, $end)->days;
    }

    /**
     * Devuelve el datetime correspondiente a la zona horaria indicada
     */
    public static function timezone(\DateTime $date, string $timezone = 'America/Bogota'): \DateTime
    {
        $nDate = clone $date;
        $nDate->setTimezone(new \DateTimeZone($timezone));
        return $nDate;
    }

    /**
     * Recibe un string de fecha y hora de un timezone origen y devuelve un DateTime para un timezone destino
     */
    public static function stDateTZ(string $strDate = null, string $timezoneO = 'America/Bogota', string $timezoneD = 'America/Bogota'): \DateTime
    {
        $date = self::date($strDate, 'DateTime', $timezoneO);
        return self::timezone($date, $timezoneD);
    }

    public static function getDays()
    {
        $days = [];
        for ($i=1; $i <= 31; $i++) {
            $days[] = $i < 10 ? "0{$i}" : $i;
        }
        return $days;
    }

    /**
     * Devuelve los días, meses y años para crear un formulario con selects para elegir fechas
     * Particularmente para formularios de fechas de nacimiento
     */
    public static function parametersForDatesForm(string $ageMin, string $ageMax, string $lng = 'es', string $date = null): array
    {
        return [
            'days' => self::getDays(),
            'months' => self::getArrayMonths($lng),
            'years' => self::yearsRange($ageMin, $ageMax, 'DESC', $date)
        ];
    }
}