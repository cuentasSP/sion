<?php
namespace Sion;

/**
 * Los que confían en el Señor están seguros como el monte Sión; no serán vencidos, 
 * sino que permanecerán para siempre.
 * Salmos 125:1
 */

use Sion\ClientSion;
use Sion\HttpClientSion;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Clase abstracta para extender clases que permitan comunicarse por las APIs de 
 * los microservicios de Su Presencia.
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
abstract class AbstractApiClientSion
{
    protected $parameter;
    protected $httpClient;
    protected $client;
    protected $request;
    protected $token;

    public function __construct(
        ParameterBagInterface $parameter, 
        HttpClientSion $httpClient, 
        ClientSion $client, 
        RequestStack $request
    ) {
        $this->parameter = $parameter;
        $this->httpClient = $httpClient;
        $this->client = $client;
        $this->request = $request;
    }

    /**
     * Implementar como obtener el header SP-APP 
     * Por lo general será desde una variable de entorno 
     */
    abstract protected function setToken(): void;

    /**
     * Implementar host base de la API 
     */
    abstract protected function getHost(): string;

    /**
     * Devuelve valor de SP-APP
     */
    protected function getToken(): string
    {
        if (!$this->token) {
            $this->setToken();
        }
        return $this->token;
    }

    /**
     * Devuelve headers estandar para comunicar entre microservicios 
     */
    protected function getHeaders(string $key = null): array
    {
        $headers = [
            'X-CLIENT-IP' => ClientSion::ipClient()
        ];
        $headers = $key == 'spapp' ? \array_merge($headers, ['SP-APP' => $this->getToken()]) : \array_merge($headers, ['X-AUTH-TOKEN' => $this->request->getSession()->getId()]);
        return ['headers' => $headers];
    }

    /**
     * Devuelve el último error de httpClient
     */
    public function getError()
    {
        return $this->httpClient->getError();
    }

    /**
     * Devuelve el contenido de la ultima respuesta de la api
     */
    public function getContent()
    {
        return $this->httpClient->getContent();
    }

    /**
     * Consume por GET una URL de una API. 
     */
    public function get(string $uri, string $tagUserHeader = null): bool
    {
        return $this->httpClient->send('GET', $this->getHost().$uri, $this->getHeaders($tagUserHeader));
    }

    /**
     * Consume por POST una URL de una API. 
     */
    public function post(string $uri, array $data = [], string $tagUserHeader = null): bool
    {
        $content = \array_merge($this->getHeaders($tagUserHeader), ['form_params' => $data]);
        return $this->httpClient->send('POST', $this->getHost().$uri, $content);
    }

    /**
     * Consume por PUT una URL de una API. 
     */
    public function put(string $uri, array $data = [], string $tagUserHeader = null): bool
    {
        $content = \array_merge($this->getHeaders($tagUserHeader), ['form_params' => $data]);
        return $this->httpClient->send('POST', $this->getHost().$uri, $content);
    }

    /**
     * Consume por DELETE una URL de una API. 
     */
    public function delete(string $uri, string $tagUserHeader = null): bool
    {
        return $this->httpClient->send('GET', $this->getHost().$uri, $this->getHeaders($tagUserHeader));
    }

    /**
     * Intenta un GET si responde intenta devolver la respuesta como un array si $inArray es true o string
     */
    public function getData(string $uri, string $tagUserHeader = null, bool $inArray = true)
    {
        return $this->get($uri, $tagUserHeader) ? $this->httpClient->getContent($inArray) : null;
    }

    /**
     * Intenta un POST si responde intenta devolver la respuesta como un array si $inArray es true o string
     */
    public function postData(
        string $uri, 
        array $data = [], 
        string $tagUserHeader = null, 
        bool $inArray = true
    ) {
        return $this->post($uri, $data, $tagUserHeader) ? $this->httpClient->getContent($inArray) : null;
    }

    /**
     * Intenta un PUT si responde intenta devolver la respuesta como un array si $inArray es true o string
     */
    public function putData(
        string $uri, 
        array $data = [], 
        string $tagUserHeader = null, 
        bool $inArray = true
    ) {
        return $this->put($uri, $data, $tagUserHeader) ? $this->httpClient->getContent($inArray) : null;
    }
}
