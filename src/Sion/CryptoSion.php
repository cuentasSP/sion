<?php
namespace Sion;

/**
 * Pero tú, Daniel, mantén en secreto esta profecía; sella el libro hasta el tiempo del fin, 
 * cuando muchos correrán de aquí para allá y el conocimiento aumentará.
 * Daniel 12:4 NTV
 * 
 * ¡Glorifica al Señor, oh Jerusalén!
 *    ¡Alaba a tu Dios, oh Sion!
 *  Pues él ha reforzado las rejas de tus puertas
 *    y ha bendecido a tus hijos que habitan dentro de tus murallas.
 * Salmo 147:12-13 NTV
 *  
 * Metodos para cifrado y descifrado simétrico utilizando la biblioteca sodium_crypto_secretbox de PHP.
 * Los parametros para usar esta libería son:
 * 1. key_secret (llave privada: binario de longitud de 32bytes almacenado como un string hexadecimal)
 * 2. nonce (valor único: valor binario aleatorio de único uso, se recibe como string hexadecimal para desencriptar) 
 * 
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */

class CryptoSion
{
    public static $error = '';

    /**
     * Encripta $plaintext, retorna array con encrypted (plaintext encriptado) y el nonce (valor único) con el que se encripto
     */
    public static function encrypt(string $plaintext, string $keySecretHex): array
    {
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);
        return [
            'encrypted' => bin2hex(sodium_crypto_secretbox($plaintext, $nonce, hex2bin($keySecretHex))),
            'nonce' => bin2hex($nonce)
        ]; 
    }

    /**
     * Desencripta $encrypted a partir del nonce (valor único) y el keySecret (llave privada)
     * Importante: nonce y keySecret se reciben como strings hexadecimales para convertirlos a binarios
     */
    public static function decrypt(string $encrypted, string $nonce, string $keySecretHex)
    {
        try {
            $plaintext = sodium_crypto_secretbox_open(
                hex2bin($encrypted), 
                hex2bin($nonce), 
                hex2bin($keySecretHex)
            );
            if ($plaintext === false) {
                static::$error = 'Error al desencriptar el mensaje.';    
            }
        } catch (\Exception $e) {
            $plaintext = false;
            static::$error = $e->getMessage();
        }
        return $plaintext;
    }
}