<?php
namespace Sion;

/**
 * Desde el monte Sión, la perfección de la belleza,
 * Dios brilla con un resplandor glorioso.
 * Salmo 50:2
 */

use Doctrine\DBAL\DBALException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Clase con metodos para ejecutar SQL directamente sobre el motor de base de datos
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class SqlSion
{
    private $doctrine;

    public $stringQuery = '';

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @throws DBALException
     */
    public function fetchQuery(string $query, string $connectionName = null, array $params = []): array
    {
        return !empty($query) ? $this->doctrine->getConnection($connectionName)->executeQuery($query, $params)->fetchAll() : [] ;
    }

    /**
     * @throws DBALException
     */
    public function execute(string $query, string $connectionName = null, array $params = []): void
    {
        if (!empty($query)) {
            $this->doctrine->getConnection($connectionName)->executeQuery($query, $params);
        }
    }

    /**
     * Crea una sentancia de tipo columna = valor o columna IN(valores)
     */
    public static function sentenceType(string $key, $value): string
    {
        if (\is_array($value)) {
            return "{$key} IN(". \implode(',', $value) .')';
        } else {
            return "{$key} = '{$value}'";
        }
    }

    /**
     * Permite crear una sentencia OR ej. (columnaA = valor OR columbaB = valor)
     */
    public static function addOR(array $keys, $value): string
    {
        $or = [];
        foreach ($keys as $key) {
            $or[] = self::sentenceType($key, $value);
        }
        return count($or) ? '(' . \implode(' OR ', $or) .')': '';
    }

    /**
     * Devuelve una sentencia WHERE de acuerdo a los $keys y valores que estan en $by
     */
    public static function where(array $keys, array $by): string
    {
        $sentence = [];
        foreach ($keys as $key => $attr) {
            if (!empty($by[$key]) && \is_string($attr)) {
                $sentence[] = self::sentenceType($attr, $by[$key]);
            } elseif (!empty($by[$key]) && \is_array($attr)) {
                $sentence[] = self::addOR($attr, $by[$key]);
            }
        }
        return count($sentence) ? 'WHERE ' . \implode(' AND ', $sentence) : '';
    }

    /**
     * Evalua si viene un where personalizado o los keys y values para intentar armar un where
     */
    public static function whereByParameters(array $parameters): string
    {
        if (!empty($parameters['where']) && \is_string($parameters['where'])) {
            return $parameters['where'];
        }
        if (!empty($parameters['keys']) && !empty($parameters['values'])) {
            return self::where($parameters['keys'], $parameters['values']);
        }
        return '';
    }

    /**
     * Si encuentra el keyJoin en el schema entonces agrega la informacion en INNER o LEFT según type
     */
    public static function addInnerLeft(array $schema, string $type, array &$query, string $keyJoin)
    {
        $joinType = ['inner' => 'INNER JOIN', 'left' => 'LEFT JOIN'];
        if (!empty($schema[$keyJoin]) && ($join = $schema[$keyJoin])) {
            $query[$type]['cls'] .= !empty($join['cls']) ? ", {$join['cls']}" : '';
            if (\is_string($join['tbs'])) {
                $join['tbs'] = [$join['tbs']];
            }
            foreach ($join['tbs'] as $tbJoin) {
                $query[$type]['tbs'] .= "{$joinType[$type]} {$tbJoin} ";
            }
            # cargar parametros al query si la receta los tiene. Si hay varios con prm solo que el último.
            if (!empty($join['prm']) && \is_string($join['prm'])) {
                $query['prm'] = $join['prm'];
            }
        }
    }

    /**
     * Recibe un $schema general y arma un array Query empezando por el que se define como FROM
     * Busca en $schema y agrega los inner & left como lo indica la receta ($recipe)
     */
    public static function buildQuery(array $schema, array $from, array $recipe): array
    {
        $query = [
            'from'  => $from,
            'inner' => ['cls' => '', 'tbs' => ''],
            'left'  => ['cls' => '', 'tbs' => '']
        ];
        if (!empty($recipe['inner']) && \is_array($recipe['inner'])) {
            foreach ($recipe['inner'] as $itm) {
                self::addInnerLeft($schema, 'inner', $query, $itm);
            }
        }
        if (!empty($recipe['left']) && \is_array($recipe['left'])) {
            foreach ($recipe['left'] as $itm) {
                self::addInnerLeft($schema, 'left', $query, $itm);
            }
        }
        if (!empty($recipe['where'])) {
            $query['where'] = $recipe['where']; 
        }
        # Si hay otros items de la receta que tengan prm, este será reemplazado por el último item agregado
        if (!empty($recipe['prm'])) {
            $query['prm'] = $recipe['prm']; 
        }
        return $query;
    }

    /**
     * Genera un query UPDATE tabla1 t1, tablaN tn SET ... WHERE ... 
     * para manejar varias tablas si se requiere el parameters[where] 
     */
    public static function buildUpdate(array $tbs, array $cls, array $parameters): string
    {
        $where = self::whereByParameters($parameters);
        $set = [];
        foreach ($cls as $key => $value) {
            $set[] = $value === NULL ? "{$key} = NULL" : "{$key} = '{$value}'";
        }
        return count($set) && $where ? "UPDATE ". \implode(',', $tbs) ." SET ". \implode(',', $set) ." {$where}" : "";
    }

    /**
     * Valida si $qry tiene el $type inner | left, si existe valida que tengan cls & tbs 
     * Devuelve un array con la estructura completa ['cls' => ..., 'tbs' => ...]
     */
    public function itemArrayQuery(string $type, array $qry): array
    {
        if (isset($qry[$type]) && is_array($qry[$type])) {
            if (!empty($qry[$type]['tbs']) && !empty($qry[$type]['cls'])) {
                return $qry[$type];
            } elseif (!empty($qry[$type]['tbs'])) {
                return ['cls' => '', 'tbs' => $qry[$type]['tbs']];
            } elseif (!empty($qry[$type]['cls'])) {
                return ['cls' => $qry[$type]['cls'], 'tbs' => ''];
            }
        }
        return ['cls' => '', 'tbs' => ''];
    }

    /**
     * Valida si $qry[$type] es string y lo devuelve, sino devuelve vacío
     */
    public function itemStringQuery(string $type, array $qry): string
    {
        return !empty($qry[$type]) && !is_array($qry[$type]) ? $qry[$type] : '';
    }

    /**
     * Recibe un array query y le agrega inner, left y reemplaza si se pide reemplazar
     */
    public static function build(array &$query, array &$q): void
    {
        if (isset($q['replace']) && is_array($q['replace'])) {
            $query = \array_merge($query, $q['replace']);
            unset($q['replace']);
        }
        if (isset($q['inner'])) {
            self::add($query['inner'], $q['inner']);
            unset($q['inner']);
        }
        if (isset($q['left'])) {
            self::add($query['left'], $q['left']);
            unset($q['left']);
        }
    }

    /**
     * Recibe un array query y devuelve un string SELECT 
     */
    public function query(array $qry): string
    {
        $from = $this->itemArrayQuery('from', $qry);
        if (!empty($from['cls']) && !empty($from['tbs'])) {
            $inner = $this->itemArrayQuery('inner', $qry);
            $left = $this->itemArrayQuery('left', $qry);
            $where = $this->itemStringQuery('where', $qry);
            $prm = $this->itemStringQuery('prm', $qry);
            $this->stringQuery = "SELECT $from[cls]  $inner[cls]  $left[cls] 
                                    FROM  $from[tbs]  
                                    $inner[tbs]  
                                    $left[tbs]
                                    $where $prm";
            return $this->stringQuery;
        }
        return '';
    }

    /**
     * Agrega al array (inner o left) valores tbs y cls de $q
     */
    public static function addQuery(array &$sb, array $q = null): void
    {
        self::add($sb, $q);
    }

    /**
     * Agrega valores a cls y tbs al apuntador $sb
     */
    public static function add(array &$sb, array $q = null): void
    {
        if (empty($sb['cls'])) {
            $sb['cls'] = '';
        }
        if (empty($sb['tbs'])) {
            $sb['tbs'] = '';
        }
        if (empty($q['cls'])) {
            $q['cls'] = '';
        }
        if (empty($q['tbs'])) {
            $q['tbs'] = '';
        }
        $sb['cls'] = "$sb[cls]  $q[cls]";
        $sb['tbs'] = "$sb[tbs]  $q[tbs]";
    }

    /**
     * Recibe el apuntador $strQ, si existe el $key en el array $q y el valor es un string lo agrega a $strQ
     */
    public static function addStr(array &$strQ, string $key, array $q): void
    {
        if (!empty($q[$key]) && \is_string($q[$key])) {
            $strQ[$key] = $q[$key];
        }
    }

    /**
     * Evalua si $query es un string | array
     * Si es un string lo devuelve, si es un array lo transforma en string con el método query
     */
    public function stringQuery($query): string
    {
        if (\is_array($query) && $query) {
            return $this->query($query);
        }
        if (\is_string($query)) {
            return $query;
        }
        return '';
    }

    /**
     * Devuelve todo el resultado de ejecutar $query
     */
    public function fetchAll($query, string $connectionName = null, array $params = []): array
    {
        return $this->fetchQuery($this->stringQuery($query), $connectionName, $params);
    }

    /**
     * Devuelve la primera posición del resultado de ejecutar $query
     */
    public function fetchOne($query, string $connectionName = null, array $params = []): array
    {
        if ($data = $this->fetchQuery($this->stringQuery($query), $connectionName, $params)) {
            return $data[0];
        }
        return [];
    }
}