<?php 
namespace Sion;

/**
 * ¿Quién vendrá del monte Sión para rescatar a Israel?
 * Cuando el Señor restaure a su pueblo, 
 * Jacob gritará de alegría e Israel se gozará.
 * Salmo 14:7
 */

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Librería para crear clientes Rest Full
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class HttpClientSion
{
    private $client;
    private $error;
    private $response;
    public $content;

    /**
     * Devuelve un cliente GuzzleHttp
     */
    public function getClient(): Client
    {
        return $this->client ? $this->client : $this->client = new Client(['verify' => false ]);
    }

    /**
     * Intentar enviar una petición http por un método
     */
    public function send(string $method, string $uri, array $options = []): bool
    {
        try {
            $this->response = $this->getClient()->request($method, $uri, $options);
            return true;
        }catch (RequestException $e) {
            $this->error = $e;
        }
        return false;
    }

    /**
     * Devolver el último error
     */
    public function getError(): ?RequestException
    {
        return $this->error;
    }

    /**
     * Devolver la última respuesta
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Si hubo un resultado en el último llamado carga la propiedad content con la respuesta y 
     * retrona el valor.
     */
    public function getContent(bool $decode = true)
    {
        if ($this->response) {
            $this->content = $this->response->getBody()->getContents();
            return $decode ? \json_decode($this->content, true) : $this->content;
        }
        return null;
    }

    /**
     * Para devolver el contenido raw 
     */
    public function getStringContent(): ?string
    {
        return $this->getContent(false);
    }
}