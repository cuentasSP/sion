<?php
namespace Sion;

/**
 * Entonces el Señor me dijo:
 *  «Escribe mi respuesta con claridad en tablas,
 *     para que un corredor pueda llevar a otros el mensaje sin error.
 *  Esta visión es para un tiempo futuro.
 *  Describe el fin, y este se cumplirá.
 *  Aunque parezca que se demora en llegar, espera con paciencia,
 *   porque sin lugar a dudas sucederá.
 *  No se tardará.
 *  Habacuc 2:2-3
 */

use Twig\Environment;
use Twig\Loader\ArrayLoader;

/**
 * Clase para renderizar usando twig 
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class RenderSion 
{
    private $twig;

    public function __construct(Environment $twig){
        $this->twig = $twig;
    }

    /**
     * Devuelve la renderización de una plantilla twig
     */
    public function render(string $path, array $data = [])
    {
        return $this->twig->render($path, $data);
    }

    /**
     * Devuelve la renderización de un contenido twig ($dataTwig)
     */
    public function renderVar($dataTwig, array $data = []): string
    {
        $env = new Environment(new ArrayLoader(['nombrePlantillaTemp' => $dataTwig]));
        return $env->render('nombrePlantillaTemp', $data);
    }
}