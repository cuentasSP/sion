<?php
namespace Sion;

/**
 * Es alto y magnífico;
 * ¡toda la tierra se alegra al verlo!
 * ¡El monte Sión, el monte santo,
 * es la ciudad del gran Rey!
 * Salmo 48:2
 */

use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Conjunto de metodos para trabajar con las entities ORM-Doctrine
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */

class EntitySion
{
    private $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    /**
     * @return ObjectManager|EntityManager
     */
    public function getEntityManager(string $name = null)
    {
        # Obtener el entityManager
        $entityManager = $this->managerRegistry->getManager($name);
        # Evaluar si está abierto
        if ($entityManager->isOpen()) {
            return $entityManager;
        }
        # si no entonces resetear 
        return $this->managerRegistry->resetManger();
    }

    /**
     * Devuelve un objeto tipo entity
     */
    public function find(string $entityClass, string $id, string $nameORM = null)
    {
        return $this->getEntityManager($nameORM)->getRepository($entityClass)->find($id);
    }

    /**
     * Devuelve una colleccion de objetos tipo entity
     */
    public function findBy(string $entityClass, array $by, string $nameORM = null)
    {
        return $this->getEntityManager($nameORM)->getRepository($entityClass)->findBy($by);
    }
    
    /**
     * Devuelve un objeto tipo entity 
     */
    public function findOneBy(string $entityClass, array $by, string $nameORM = null)
    {
        return $this->getEntityManager($nameORM)->getRepository($entityClass)->findOneBy($by);
    }

    /**
     * Todos los objetos entity
     */
    public function findAll(string $entityClass, string $nameORM = null)
    {
        return $this->getEntityManager($nameORM)->getRepository($entityClass)->findAll();
    }

    /**
     * Devuelve el resultado de ejecutar un query DQL con objetos tipo entity
     */
    public function createQuery(string $dql)
    {
        $query = $this->getEntityManager()->createQuery($dql);
        return $query->execute();
    }

    /**
     * @throws ORMException
     */
    public function persist(object $entity, string $nameORM = null): void
    {
        $this->getEntityManager($nameORM)->persist($entity);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws MappingException
     */
    public function flush(string $nameORM = null): void
    {
        $em = $this->getEntityManager($nameORM);
        $em->flush();
        $em->clear();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(object $entity, string $nameORM = null)
    {
        $em = $this->getEntityManager($nameORM);
        $em->persist($entity);
        $em->flush();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(object $entity, string $nameORM = null)
    {
        $em = $this->getEntityManager($nameORM);
        $em->remove($entity);
        $em->flush();
    }
}