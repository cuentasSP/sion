<?php
namespace Sion;

/**
 * Desde el monte Sión, la perfección de la belleza,
 * Dios brilla con un resplandor glorioso.
 * Salmo 50:2
 * 
 * Metodos para evaluar si la data cumple con los requisitos 
 * @author Walther Bojacá <wbojaca@supresencia.com>
 */
class CheckDataSion
{
    public static $messages = [];

    /**
     * Si la acción es crear deja el data igual.
     * Si la acción es actualizar cambia el atributo require por false porque son obligatorios unicamente para crear
     */
    public static function configDataCheck(string $actionType, array $data): array
    {
        $dataCheck = [];
        foreach ($data as $key => $value) {
            $value['required'] = $actionType == 'create' ? $value['required'] : false;
            $dataCheck[$key] = $value;
        }
        return $dataCheck;
    }

    /**
     * Validar si la data cumple con los requisitos de $checks
     */
    public static function checkData(array $checks, array &$data): bool
    {
        $response = true;
        self::$messages = [];
        foreach ($checks as $key => $chk) {
            self::checkItem($response, $data, $key, $chk);
        }
        return $response;
    }

    /**
     * Validar el item contra los parametros de $eval
     */
    public static function checkItem(bool &$response, array &$data, string $key, array $eval): void
    {
        if ($eval['required'] && !isset($data[$key])) {
            self::$messages[] = "No existe {$key}, es requerido.";
            $response = false;
        }
        if (isset($data[$key]) && !self::checkType($eval['type'], $data[$key])) {
            self::$messages[] = "{$key} no es de tipo {$eval['type']}.";
            $response = false;
        }
        if (isset($data[$key]) && !empty($eval['length']) && \strlen($data[$key]) > $eval['length']) {
            self::$messages[] = "{$key} supera el ancho ({$eval['length']}) permitido.";
            $response = false;
        }
    }

    /**
     * Evaluar si hay un tipo de dato dato tiene metodo para evaluar, sino devuelve true
     */
    public static function checkType(string $type, $value): bool
    {
        $method = "eval_{$type}";
        if (\method_exists(self::class, $method)) {
            return self::$method($value);
        }
        return true;
    }

    /**
     * Evaluar si es un string
     */
    public static function eval_string($value): bool
    {
        return \is_string($value);
    }

    /**
     * Evaluar si es un email
     */
    public static function eval_email($value): bool
    {
        return ToolsSion::isEmail($value);
    }

    /**
     * Evaluar si es una fecha
     */
    public static function eval_date($value): bool
    {
        return DateSion::isDate($value, 'Y-m-d');
    }

    /**
     * Evaluar si es un DateTime
     */
    public static function eval_datetime($value): bool
    {
        return DateSion::isDate($value, 'Y-m-d H:i:s');
    }

    /**
     * Evaluar si es un número
     */
    public static function eval_int($value): bool
    {
        return \is_numeric($value);
    }

    /**
     * Evaluar si es un array
     */
    public static function eval_array($value): bool
    {
        return \is_array($value);
    }

    /**
     * Evaluar si es un booleano o tiene un valor string de tipo booleano (0-1 o false-true)
     */
    public static function eval_bool($value): bool
    {
        $value = ($value === '0' || $value === 'false') ? $value = false: $value; 
        $value = ($value === '1' || $value === 'true')  ? $value = true : $value;
        return \is_bool($value);
    }
}